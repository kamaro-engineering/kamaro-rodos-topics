add_library(kamaro-rodos-topics STATIC
  src/acceleration_topic.cpp
  src/angle_topics.cpp
  src/attitude_topic.cpp
  src/battery_voltage_topic.cpp
  src/button_topics.cpp
  src/discovery_led_topics.cpp
  src/ground_nav_sat_topics.cpp
  src/imalive_topic.cpp
  src/main_motor_topic.cpp
  src/ping_topic.cpp
  src/servo_topics.cpp
  src/tracked_thrust_topic.cpp
  src/tutorial_motor.cpp
  src/tutorial_pose.cpp
  src/tutorial_twist_command.cpp
  src/tutorial_twist_imu.cpp
  src/twist_chassis_topic.cpp
  src/velocity_topic.cpp
)

target_include_directories(kamaro-rodos-topics PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
)

target_link_libraries(kamaro-rodos-topics PUBLIC
  kamaro-rodos-messages
)
