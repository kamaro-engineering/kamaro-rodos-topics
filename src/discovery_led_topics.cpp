#include "discovery_led_topics.hpp"

Topic<led> discovery_led_blue(1301, "discovery_led_blue");
Topic<led> discovery_led_green(1302, "discovery_led_green");
Topic<led> discovery_led_orange(1303, "discovery_led_orange");
Topic<led> discovery_led_red(1304, "discovery_led_red");
