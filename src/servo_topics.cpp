#include "servo_topics.hpp"

Topic<angle> front_servo_motor_status_angle(1161, "front_servo_motor_status_angle");
Topic<rpm> front_servo_motor_status_rpm_des(1162, "front_servo_motor_status_rpm_des");
Topic<rpm> front_servo_motor_status_rpm_real(1163, "front_servo_motor_status_rpm_real");
Topic<current> front_servo_motor_status_current(1164, "front_servo_motor_status_current");


Topic<angle> back_servo_motor_status_angle(1171, "back_servo_motor_status_angle");
Topic<rpm> back_servo_motor_status_rpm_des(1172, "back_servo_motor_status_rpm_des");
Topic<rpm> back_servo_motor_status_rpm_real(1173, "back_servo_motor_status_rpm_real");
Topic<current> back_servo_motor_status_current(1174, "back_servo_motor_status_current");