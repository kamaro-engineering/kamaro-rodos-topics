#include "button_topics.hpp"

Topic<button_pressed> button_topic(1305, "button-topic");
Topic<button_short_pressed> button_short_topic(1306, "button-short-topic");
Topic<button_long_pressed> button_long_topic(1307, "button-long-topic");
