#include "tutorial_motor.hpp"

Topic<motor_thrust> tutorial_motor_left(1201, "tutorial_motor_left");
Topic<motor_thrust> tutorial_motor_left_executed(1204, "tutorial_motor_left_executed");

Topic<motor_thrust> tutorial_motor_right(1202, "tutorial_motor_right");
Topic<motor_thrust> tutorial_motor_right_executed(1205, "tutorial_motor_right_executed");
