#include "main_motor_topic.hpp"

Topic<rpm> main_motor_rpm_topic(1150, "main_motor_rpm_topic");
Topic<rpm> main_motor_status_rpm(1151, "main_motor_status_rpm");
Topic<current> main_motor_status_current(1152, "main_motor_status_current");
Topic<current> main_motor_status_targetCurrent(1153, "main_motor_status_current");
Topic<current> main_motor_status_commandCurrent(1154, "main_motor_status_current");