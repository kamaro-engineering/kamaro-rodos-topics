from pathlib import Path
import re

pattern = re.compile(r'\((\d+),\s*"([\w-]+)"\);')

topic_files = (Path(__file__).parent/'src/').rglob('*.cpp')

topics = dict()

for file_name in topic_files:
    with open(file_name, 'r') as topic_file:
        result = pattern.findall(topic_file.read())
        for match in result:
            topics[int(match[0])]=match[1]

usr_input =  input("sort topics by [i]d or [n]ame? ")
if usr_input == "i":
    for key, value in sorted(topics.items(), key=lambda x: x[0]): 
        print("{} : {}".format(key, value))
elif usr_input == "n":
    for key, value in sorted(topics.items(), key=lambda x: x[1]): 
        print("{} : {}".format(key, value))