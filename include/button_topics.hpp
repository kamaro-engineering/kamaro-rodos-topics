#pragma once
#include "rodos.h"
#include "button_msg.hpp"

extern Topic<button_pressed> button_topic;
extern Topic<button_short_pressed> button_short_topic;
extern Topic<button_long_pressed> button_long_topic;
