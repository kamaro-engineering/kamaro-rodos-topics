#pragma once
#include "rodos.h"
#include "rpm_msg.hpp"
#include "current_msg.hpp"

extern Topic<rpm> main_motor_rpm_topic;
extern Topic<rpm> main_motor_status_rpm;
extern Topic<current> main_motor_status_current;
extern Topic<current> main_motor_status_targetCurrent;
extern Topic<current> main_motor_status_commandCurrent;