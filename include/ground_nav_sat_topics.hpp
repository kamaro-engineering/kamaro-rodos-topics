#pragma once
#include "rodos.h"
#include "nav_sat_fix.hpp"
#include "nav_sat_track.hpp"

extern Topic<nav_sat_fix> ground_nav_sat_fix_topic;
extern Topic<nav_sat_track> ground_nav_sat_track_topic;
