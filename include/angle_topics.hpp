#pragma once
#include "rodos.h"
#include "angle_msg.hpp"

extern Topic<angle> angle_front_topic;
extern Topic<angle> angle_back_topic;