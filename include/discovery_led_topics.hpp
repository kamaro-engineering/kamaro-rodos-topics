#pragma once
#include "rodos.h"
#include "led_msg.hpp"

extern Topic<led> discovery_led_blue; // LD6, PD15
extern Topic<led> discovery_led_green; // LD4, PD12
extern Topic<led> discovery_led_orange; // LD3, PD13
extern Topic<led> discovery_led_red; //  LD5, PD14
