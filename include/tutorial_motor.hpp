#pragma once
#include "rodos.h"
#include "motor_thrust_msg.hpp"

extern Topic<motor_thrust> tutorial_motor_left;
extern Topic<motor_thrust> tutorial_motor_left_executed;

extern Topic<motor_thrust> tutorial_motor_right;
extern Topic<motor_thrust> tutorial_motor_right_executed;
