#pragma once
#include "rodos.h"
#include "twist_msg.hpp"

extern Topic<twist> tutorial_twist_command;
extern Topic<twist> tutorial_twist_command_executed;
