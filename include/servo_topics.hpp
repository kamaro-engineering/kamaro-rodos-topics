#pragma once
#include "rodos.h"
#include "rpm_msg.hpp"
#include "current_msg.hpp"
#include "angle_msg.hpp"

extern Topic<angle> front_servo_motor_status_angle;
extern Topic<rpm> front_servo_motor_status_rpm_des;
extern Topic<rpm> front_servo_motor_status_rpm_real;
extern Topic<current> front_servo_motor_status_current;

extern Topic<angle> back_servo_motor_status_angle;
extern Topic<rpm> back_servo_motor_status_rpm_des;
extern Topic<rpm> back_servo_motor_status_rpm_real;
extern Topic<current> back_servo_motor_status_current;